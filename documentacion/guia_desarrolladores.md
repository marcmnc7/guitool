# Documentación para desarrolladores

## 1. Objetivos principales del proyecto GUI-Tool

1. Creación de múltiples herramientas básicas: Crear un número suficiente de herramientas básicas cómo para sea factible utilizar la plataforma de manera regular por un administrador de sistemas.

2. Creación de herramientas más complejas y útiles: Conseguir que las herramientas desarrolladas sean realmente útiles por los administradores del sistema. En este caso, se entiende por “herramienta útil” la que permita crear o administrar algo que haciéndolo cada cierto tiempo por línea de comandos resulta costoso, tanto por el hecho de recordar la metodología y los pasos como por el hecho de codificarlos.

3. Creación de una plataforma web bonita: Crear una plataforma web que esté muy bien diseñada y que sea intuitiva para el usuario. Todo implementado con Django y su potente sistema de templates.

4. Creación de una plataforma web funcional: Conseguir que la plataforma web desarrollada sea eficiente y rápida gracias a la correcta integración del código HTML/CSS al sistema de templates y también a la correcto codificación de la lógica interna de Django con Python.

## 2. Tecnologias y requisitos de conocimiento

A continuación se listan buena parte de las tecnologías y los conocimientos que se requieren para ayudar en el desarollo del proyecto GUI-Tool.

- **Django**

Es la pieza angular del proyecto. Conecta la parte lógica del sistema (scripts modificadores de nuestro sistema) con la parte lógica propia (views - código para procesar los datos que se envían de los formularios) y con la parte visual (código HTML - CSS). Además, se utiliza el servidor web que trae incorporado. Todo en uno.

- **Python**

Lenguaje con que se desarrolla todo el código de Django. Las views.

- **Bash**

Lenguaje con que se desarrollarán los *scripts* que modificarán el sistema, que harán algunas las comprobaciones de requerimientos de las herramientas y que nos permitirán también hacer el despliegue y puesta en funcionamiento de la plataforma de forma rápida en un host.

- **Protocolo HTTP**

Protocolo que es necesario conocer para gestionar la comunicación entre el framework web y el navegador y así poder procesar los datos que provienen de los formularios enviados desde el lado "cliente". Además, es importante conocer aspectos como las cookies para mantener información entre sesiones.

- **HTML / CSS**

Lenguajes con los cuales se diseña el estilo y la estructura de la pagina web.

- **Otras tecnologías específicas**

Según cual es la herramienta que se desea desarrollar, se necesita un profundo conocimiento sobre esta para poderla desarrollar de forma correcta. Según las herramientas que ya se incorporan, algunas tecnologías són: Dockers, Sql, aplicaciones de bash, etc.

- **Otras tecnologías generales**

	* Git
	* Entornos virtuales
	* ...

## 3. Estructura del repositorio

A continuación se presenta el arbol de todo el proyecto GUI-Tool for Linux System Adminsitration y se explica de forma mas o menos detallada su contenido más importante.

	.
	├── activate_env.sh
	├── documentacion
	│   ├── dietari.md
	│   ├── faq.md
	│   ├── guia_desarrolladores.md
	│   ├── guia_usuario.md
	│   ├── planificacio.md
	│   └── presentacion
	├── for_developing
	│   ├── install_guitool.sh
	│   ├── sheet_commands.sh
	│   └── start_developing.sh
	├── GUIT_LinuxAdministration [Foo](#carpeta:-guit_linuxadministration)
	│   ├── db.sqlite3
	│   ├── GUIT_LinuxAdministration
	│   │   ├── admin.py
	│   │   ├── __init__.py
	│   │   ├── migrations
	│   │   ├── models.py
	│   │   ├── settings.py
	│   │   ├── urls.py
	│   │   ├── views.py
	│   │   └── wsgi.py
	│   ├── manage.py
	│   ├── plantillas
	│   │   ├── base.html
	│   │   ├── create_backup.html
	│   │   ├── create_bbdd.html
	│   │   ├── create_docker.html
	│   │   ├── create_user.html
	│   │   ├── generic.html
	│   │   └── search_tool.html
	│   └── static
	│       ├── assets
	│       │   ├── css
	│       │   ├── fonts
	│       │   ├── js
	│       │   └── sass
	│       └── images
	├── README.md
	├── requirements.txt
	├── script_programs
	│   ├── create_backups.sh
	│   ├── create_bbdd.sh
	│   ├── create_docker.sh
	│   ├── create_user.sh
	│   └── run_backup.sh
	├── start_app.sh
	└── venv

### 3.1 Carpeta: GUIT_LinuxAdministration

Esta carpeta contiene todas las aplicaciones del proyecto Django en cuestión. Su estructura se crea automáticamente cuando se ejecuta `django-admin startproject GUIT_LinuxAdministration`.
Además de las subcarpetas que se explican a continuación, aquí se encuentran algunos ficheros:
- *db.sqlite3* --> Es la bbdd que por defecto utiliza Django para sus proyectos.
- *manage.py* --> Fichero con el que mediante argumentos se puede controlar el comportamiento del servidor, arrancarlo o pararlo, aplicar los cambios en la bbdd, chequearlo, etc. 

#### 3.1.1 Subcarpeta: GUIT_LinuxAdministration

El proyecto no utilitza otras aplicaciones, por lo tanto esta "todo" en dicha carpeta. Un ejemplo de aplicación que se podria externalizar seria añadir una funcionalidad en la plataforma para que los usuarios puedan añadir comentarios en cada herramienta y responder a los de otros. Esta característica de Django nos permite poder desarrollar una aplicación que se puede usar en varios proyectos indistintamente.

Aquí se encuentran ficheros que es muy importante conocer:

- *settings.py* --> Contiene todas configuraciones del proyecto. Entre ellas:
	- `INSTALLED_APPS`: Que aplicaciones usa (algunas vienen incorporadas por defecto como por ejemplo el panel de administracion `django.contrib.admin`).
	- `ROOT_URLCONF`: Define cual es el fichero de configuración de las urls (explicado en els siguiente punto).
	- `TEMPLATES`: Define el motor de plantillas que se usa y el directorio donde éstas son almacenadas.
	- `DATABASES`: Define el motor de bbdd que se usa, así como la ruta al fichero en cuestión *db.sqlite3*.
	- `LANGUAGE_CODE` y `TIME_ZONE`: Tal y como indica su nombre, define el lenguaje y la zona horaria del servidor.
	- `STATIC_URL` y `STATICFILES_DIRS`: Define la ruta y las carpetas donde se almacenarán los ficheros estáticos que utilizara la plataforma.
- *urls.py* --> Define los patrones de URL'S que la aplicación parseara, en orden, para procesar de una manera o de otra (view function) la petición del cliente.
- *views.py* --> Es el archivo que contiene el comportamiento del proyecto. En él se crean las funciones que ejecutarán el código según cual sea la petición del cliente.

Por tanto, el flujo de ejecución sigue el siguiente orden:
```
Navegador --> Request --> Urlsconf --> Views --> Render --> Navegador
```

Cada una de estas funciones `views` reciben de entrada la petición que ha realizado el cliente y devuelven una respuesta *HTTP*, que es la que sera enviada y mostrada en el navegador y contiene información de la *request*, la referencia a la página *html* (plantilla) que se tiene que mostrar y un diccionario de variables que se usara para modificar dinámicamente el contenido *html* de la plantilla que se mostrará al cliente.

	return render(request, 'create_user.html', {'groups':groups, 'messages':error_messages[4]})

En este fichero se dispone de una función `view` por cada herramienta que ofrece la plataforma. Todas éstas estan explicadas a continuación:

##### base
Se procesa cuando en la url se *matchea* `path('base/', views.base)`: [127.0.0.1:8000/base/](http://127.0.0.1:8000/base/). Procesa el comportamiento de la *"landing page"* de la plataforma.
1. Construye mediante la función propia `build_frases` una lista aleatoria de frases a mostrar en el apartado citas computacionales.
2. Devuelve la respuesta llamando a la plantilla `base.html` y con la variable `citas`, que contiene las frases aleatorias construidas en el anterior paso.

Durante todas las demás funciones se repite el mismo paso en relación a las citas. Por lo que no se comentará en ningun apartado más. 

##### create_user
Se procesa cuando en la url se *matchea* `path('create_user/', views.create_user)`: [127.0.0.1:8000/create_user/](http://127.0.0.1:8000/create_user/). Procesa el comportamiento de la herramienta de crear un usuario de la plataforma.

1. Construye mediante una función propia `popen_stdout` una lista de los grupos actuales del sistema. Se necesitan para poder mostrarlos al usuario en el formulario justo cuando entra en el apartado de ésta herrmienta.

2. Se repite el mismo proceso para los usuarios actuales del sistema. Se necessitan para realizar la comprovacion de que el usuario que se desea crear aún no exista en el sistema.

3. Cuando en el formulario (metodo `GET`) es presionado el boton de **'Crear Usuario'**:
	1. Se parsea toda la *query string* y se almacena en un diccionario.
		- Si no se dispone de los campos obligatorios (de las claves `username`, `password1`, o `password2`), se devuelve la respuesta llamando a la plantilla `create_user.html` con las variables `citas`, `groups` que contiene una lista con todos los grupos del sistema y `messages` que contiene un elemento lista de la constante `error_messages` que define los diferentes mensajes de errores que pueden ocurrir en la herramienta.
		- Si se dispone de todos los campos obligatorios, se almacena cada uno en una variable.
	2. Se almacena directamente la variable `group_main` que contiene el grupo principal al que pertenecerá el usuario ya que siempre contendra algun valor (así se ha establecido en el `.html`)
	3. Se procesan los campos opcionales del formulario.
		- Si el usuario ha especificado un valor en el campo opcional, se obtiene el valor y se almacena en una variable.
		- Si no se ha especificado, ésta misma variable se almacena con el valor 'None'. Ésto es debido a que en cualquier caso se necesita esta varibale ya que siempre se pasarán por argumento todos estos valores al *script* modificador del sistema (más adelante).
	4. Una vez obtenidas todas las variables, se procede a realizar las comprovaciones necesarias sobre ellas.
		- Que el usuario no exista actualmente en el sistema y que no sea un *string* vacio. Si es así, devuelve la respuesta con el error correspondiente y las demas variables `citas` y `groups` ya que queremos que el usuario sigue viendo las citas y pueda seguir rellenando el formulario pudiendo elegir un grupo del desplegable.
		- Que la `password1` y la `password2` no sean iguales o que sean un *string* vacio. Si es asi, devuelve la respuesta con el error correspondiente y las demás variables.
		- Si el usuario ha establecido la opción `default` para el directorio *home* del usuario, modifica la variable del directorio *home* ya que éste sera `/home/username`. En caso contrario, comprueba mediante una *regex* que la ruta que ha introducido el usuario es una ruta válida de sistemas UNIX. Si no lo es, devuelve la respues con el error correspondiente y las demás variables.
	5. Procesa todos los grupos secundarios que el usuario ha especificado y conviertelos a un formato especifico: un *string* separando cada grupo por comas. Es así ya que de esta forma será mas fácil procesar esta información por el *script* modificador del sistema.
	6. Mediante una funcion propia `popen_returncode` llama al *script* modificador del sistema pasandole por argumento todos los valores anteriores. Recoje el valor de retorno ya que, según éste, se devuelve la respuesta con el mensaje apropiado.

4. Si no se ha presionado ningun botón. Es decir, si se entra a la pagina de la herramienta por primera vez (o se actualiza), devuelve la respuesta llamando a la plantilla `create_user.html` con las variables `citas`, `groups` pero tambien con la variable `messages` a `None` ya que el comportamiento de la plantilla `.html` establecido así lo requiere para que no muestre ningún *banner*.

##### create_backup
Se procesa cuando en la url se *matchea* `path('create_backup/', views.create_backup)`: [127.0.0.1:8000/create_backup/](http://127.0.0.1:8000/create_backup/). Procesa el comportamiento de la herramienta de crear una rutina de backup.

1. Cuando en el formulario (metodo `GET`) es presionado el boton de **'Crear Backup'**:
	1. Se parsea toda la *query string* y se almacena en un diccionario.
		- Si no dispone de los campos obligatorios (de las claves `directorio` o `timing`), se devuelve la respuesta llamando a la plantilla `create_backup.html` con las variables `citas` y `messages`.
		- Si se dispone de todos los campos obligatorios, se almacenan sus valores en variables.
	2. Se procede a procesar el único campo opcional del formulario de ésta herramienta: crear ahora mismo el primer backup.
		- Si el usuario ha chequeado el campo se obtiene el valor y se almacena en una variable.
		- Si no se ha chequeado, esta misma variable se almacena con el valor 'no'. En cualquier caso se necesita ésta varibale ya que siempre se pasarán todos estos valores al *script* modificador del sistema.
	3. En este caso, a diferencia del anterior, no se realiza la comprobación de si el usuario ha introducido un directorio válido ya que esta comprovación es realizada en el *script* modificador del sistema. Dos maneras diferentes, mismo resultado.
	4. Mediante una función propia `popen_returncode` llama al *script* modificador del sistema pasándole por argumento todos los valores anteriores. Recoje el valor de retorno ya que, segun éste, se devuelve la respuesta con el mensaje apropiado.

2. Si no se ha presionado ningun botón. Es decir, si se entra a la pagina de la herramienta por primera vez (o se actualiza), devuelve la respuesta llamando a la plantilla `create_backup.html` con la varibale `citas` pero tambien con la variable `messages` a `None` ya que el comportamiento de la plantilla `.html` establecido asi lo requiere para que no muestre ningun *banner*.

##### create_docker
Se procesa cuando en la url se *matchea* `path('create_docker/', views.create_docker)`: [127.0.0.1:8000/create_docker/](http://127.0.0.1:8000/create_docker/). Procesa el comportamiento de la herramienta de crear un *container* de Docker de características específicas.

1. Mediante una funcion propia `popen_returncode` intenta arrancar el servicio de Docker. En caso negativo se devuelve la respuesta llamando a la plantilla `create_docker.html` con las variables `citas`, `networks` que tendria que contener una lista con todas las redes Docker existentes y `messages` que contiene un elemento lista de la constante `error_messages`, que define los diferentes mensajes de errores que pueden ocurrir en ésta herramienta.

2. Construye mediante una función propia `popen_stdout` una lista de las redes docker creadas actualmente en el sistema. Se necesitan para poder mostrarlas al usuario en el formulario en el momento en que entra al apartado de ésta herrmienta.

3. Cuando en el formulario (metodo `POST`) es presionado el botón de **'Crear Docker'**:
	1. Se parsea toda la *query string* y se almacena en un diccionario.
		- Si no dispone de los campos obligatorios (de la clave `so_base`, `name` y `hostname`), se devuelve la respuesta llamando a la plantilla `create_docker.html` con las variables `citas`, `networks` y `messages`.
		- Si se dispone de todos los campos obligatorios, se procede a guardarlos en variables.
	2. Si el usuario ha elegido algun paquete a instalar, mediante la funcion propia `system_particularities` se crea un *string* que será un pequeño *script* de instalación adaptado a la plataforma que haya elegido. Éste *script* será ejecutado al iniciar el *container*.
	3. Se procede a procesar los campos opcionales del formulario.
		- Si el usuario ha especificado un valor en algún campo opcional (Directorio de trabajo, Ficheros o *Privileged*), se obtiene el valor y se almacena en una variable.
		- Si no se ha especificado, ésta misma variable se almacena con el valor `'None'` (o, en el caso de la variable `privileged`, se almacena con el valor `false`) . En cualquier caso se necesitan esta varibales ya que siempre se pasaran todos estos valores al *script* modificador del sistema.
	4. Durante la obtención de todas las variables anteriores, se realizan una serie de comprovaciones necesarias sobre ellas.
		- Que el nombre del *container* o el `hostname` no sean simplemente espacios en blanco.
		- Si el usuario ha establecido la opción de empezar a trabajar en un directorio en concreto, comprueba mediante una regex que la ruta que ha introducido es una ruta válida de sistemas UNIX. Si no lo es, devuelve la respuesta con el error correspondiente y las demás variables.
	5. Mediante una sentencia `try/except` crea el directorio `/etc/guitool/tmp`, que se usará para ubicar el `Dockerfile` y los ficheros que se deseen incorporar en el *container*.
	6. Si la petición pretende incorporar ficheros al servidor, subelos y guarda cada uno de éstos en el directorio anterior.
	7. Mediante una función propia `popen_returncode` llama al *script* modificador del sistema pasandole por argumento todos los valores anteriores. Recoje el valor de retorno ya que, según éste, se devuelve la respuesta con el mensaje apropiado.

4. Si no se ha presionado ningun botón. Es decir, si se entra a la página de la herramienta por primera vez (o se actualiza), devuelve la respuesta llamando a la plantilla `create_docker.html` con las variables `citas`, `networks` pero también con la variable `messages` a `None` ya que el comportamiento de la plantilla `.html` establecido así lo requiere para que no muestre ningún mensaje.

##### create_bbdd
Se procesa cuando en la url se *matchea* `path('create_bbdd/', views.create_bbdd)`: [127.0.0.1:8000/create_bbdd/](http://127.0.0.1:8000/create_bbdd/). Procesa el comportamiento de la herramienta de crear una base de datos de *psql*.

1. Recoge la información, si existe, de las cookies `tables` (diccionario que contiene la información de todo lo que se va rellenando en el formulario. Se utiliza para mantener la estructura del `.html` entre peticiones) y `bbdd` (string que contiene la información del nombre de la base de datos que se esta creando). Si no existe, inicializa las variables vacías.

2. Se declara la variable `fields` y se guarda en ella toda la información que provenga de la petición. Esta variables es la misma que en las otras vistas, un diccionario que contiene toda la informacion del `POST`. Con los datos de este `POST` se actualiza la variable `tables`: POST --> TABLES --> HTML --> POST

3. Cuando en el formulario (metodo `POST`) es presionado el boton de **'Añadir tabla'**:
	1. Intenta encontrar el nombre de la bbdd que se esta creando, bien por información de sesión (*cookies*) o bien porque se ha rellenado en el formulario por primera vez.
	- Si no existe, devuelve la respuesta llamando a la plantilla `create_bbdd.html` con las variables `citas`, `tables`, `bbdd`, y `messages`.
	- Si existe, comprueba que el primer numero de ésta no sea un numero y que no contenga espacios. En caso negativo, devuelve la respuesta llamando a la plantilla `create_bbdd.html` con las variables `citas`, `tables`, `bbdd`, y `messages`.
	2. Obtiene el nombre de la nueva tabla creada, la guarda en la variable `table_name` y chequea que no contenga espacios, que su primer caracter no sea un numero y que contenga algún valor. En caso contrario, devuelve la respuesta con las variables y el error correspondiente.
	3. Obtiene los campos que se han especificado para esta tabla, los guarda en una lista llamada `table_campos` y chequea que exista, que no sean mas de 50, que no contengan palabras reservadas de *psql* y que ninguno empieze por numero.
	4. Comprueba que esta tabla que se esta intentando crear aún no existe en esta sesión. En caso negativo, devuelve la respuesta con las variables y el error correspondiente.
	5. Crea la sección vacía de ésta tabla en el diccionario `tables`, ya que aun no puede haber especificado nada en esta petición.
	6. Mediante la funcion propia `refresh_tables_dict` genera el diccionario `tables` para que los valores de los campos que tenga actualmente `fields` sean guardados ya que se desea que la información que ya se ha introducido este disponible cuando se vaya a añadir otra tabla.
	7. Actualiza también el valor de la *cookie* de `bbdd` para poderla obtener en la siguiente petición.
	8. Devuelve la respuesta llamando a la plantilla `create_bbdd.html` con las variables `citas`,  `tables`, `bbdd`, y `messages` referenciando el mensaje de que se ha añadido correctamente la tabla.

4. Cuando en el formulario (metodo `POST`) es presionado el botón de **'Borrar tabla'**:
	1. Obtén el nombre de la base de datos que se esta intentando borrar. 
	- Si no existe devuelve la respuesta llamando a la plantilla `create_bbdd.html` con las variables `citas`, `tables`, `bbdd`, y `messages` con el error de que no se encuentra esta tabla.
	- Si existe, borra esta información del diccionario `tables`.
	2. Actualiza las *cookies* de las tablas. No queremos que en siguientes peticiones nos venga la información de la tabla que hemos borrado.
	3. Devuelve la respuesta llamando a la plantilla `create_bbdd.html` con las variables `citas`,  `tables`, `bbdd`, y `messages` referenciando el mensaje de que se ha borrado correctamente la tabla.

5. Cuando en el formulario (metodo `POST`) es presionado el boton de **'Borrar todo'**:
	1. Borra todas las variables y las *cookies* y haz un `flush` de toda la información de la session. Se desea empezar de nuevo y por tanto no necesitamos informacion de la anterior sesion.
	2. Devuelve la respuesta llamando a la plantilla `create_bbdd.html` con las variables `citas`,  `tables`, `bbdd`, y `messages`.

6. Cuando en el formulario (metodo `POST`) es presionado el boton de 'Actualizar/Guardar':
	1. Mediante la función propia `refresh_tables_dict` actualiza el diccionario `tables` con la información que se ha introducido en el formulario.
	2. Actualiza las *cookies*.
	3. Devuelve la respuesta llamando a la plantilla `create_bbdd.html` con las variables `citas`,  `tables`, `bbdd`, y `messages`.

7. Cuando en el formulario (metodo `POST`) es presionado el boton de **'Obtener script'**:
	1. Mediante la función propia `refresh_tables_dict` actualiza el diccionario `tables`.
	2. Mediante la funcion propia `build_bbdd_script` crea el *script* de creación de la base de datos concatenando los diferentes valores que se tienen en el diccionario `fields`, es decir, en el `POST` final.
	3. Prepara y envía al cliente la respuesta que transportará el fichero llamado `script_gui.sql`, que contiene el *script* con el que el usuario podra empezar a trabajar o añadir manualmente a *psql*.

8. Cuando en el formulario (metodo `POST`) es presionado el boton de **'Crear BBDD'**:
	1. Mediante una función propia `popen_returncode` intenta arrancar el servicio de *postgresql*. En caso negativo se devuelve la respuesta llamando a la plantilla `create_bbdd.html` con las variables `citas`,  `tables`, `bbdd`, y `messages`, indicando que no se puede arrancar.
	2. Mediante la función propia `refresh_tables_dict` actualiza el diccionario `tables`.
	3. Mediante la funcion propia `build_bbdd_script` crea el *script* de creación de la BBDD concatenando los diferentes valores que se tienen en el diccionario `fields` y guardalo en un fichero en la carpeta `/tmp` llamado `script_gui.sql`. Éste fichero es el *script* que será llamado por el *script* modificador del sistema para crear la BBDD.
	4. Obtiene los valores introducidos en los campos usuario y contraseña.
	5. Mediante una funcion propia `popen_returncode` llama al *script* modificador del sistema pasándole por argumento el usuario y la contraseña. Recoje el valor de retorno.
	- Si ido todo correcto devuelve la respuesta llamando a la plantilla `create_bbdd.html` con las variables `citas`,  `tables`, `bbdd`, y `messages`, indicando que se ha creado con éxito.
	- Si ha habido algun error devuelve la respuesta llamando a la plantilla `create_bbdd.html` con las variables `citas`,  `tables`, `bbdd` y `messages`, en este caso no con un valor de la lista `error_messages`, sino con los errores que provienen del *script* modificador del sistema.

##### show_search
Se procesa cuando en la url se *matchea* `path('search_tool/', views.show_search)`. Procesa el comportamiento de la funcionalidad de buscar una herramienta en la plataforma.

1. Se parsea toda la *query string* y se almacena en un diccionario.
	- Si no se ha realizado ninguna consulta (clave `query`), se devuelve la respuesta llamando a la plantilla `search_tool.html` con las variables `citas`, `tools`. Ésta última vacía. 
	- Si, por contra, el usuario ha realizado una consulta, se procede a guardarla en una variable.
2. Mediante la siguiente orden `Herramientas.objects.filter(descripcion__icontains=query).values()` se realiza una consulta a la base de datos del admin de Django y se obtienen los valores. La consulta pretende buscar todas las herramientas que en su campo descripcion contengan el texto que el usuario ha introducido en el buscador.
3. Una vez obtenidas, se deveuelve la respuesta con las variables `citas` y `tools` con las herramientas que han coincidido con el criterio de busqueda. La plantilla las procesa y las enseña.

#### 3.1.2 Subcarpeta: plantillas

En esta carpeta se ecuentra todo el código `.html`. Cada fichero es una plantilla que se usará cuando sea invocada por el*render* de las *views*. Una plantilla puede heredar de otra y, por tanto, reutilizar una parte importante de código común. En este caso, la jerarquia que se ha montado es la siguiente:

                                                base.html
                                      +------------+----------+
                                      |                       |
                                      |                       |
                                      v                       v
                                 generic.html            search_tool.html
                                      +
                                      |
                                      |
                  +----------+--------+----+------------+
                  |          |             |            |
                  v          v             v            v
          create_user   create_backup create_docker create_bbdd

Que una plantilla herede de otra significa que importa todo el código .html de esta primera. Esto adquiere especial importancia gracias a las etiquetas, que nos permiten modificar alguna parte en concreto (todas las que queramos) de éste codigo heredado.

Segun ésta implementación, la `base.html` contiene lo que será la estructura base de toda la página web, incluyendo:
	- Barra de navegacion lateral con los *links* a las diferentes herramientas y las citas computacionales.
	- El menú superior, con el titulo, los *links* al repositorio de Git...

Algunas partes como por ejemplo el `style`, los `tags`, el titulo que tendra la herramienta y, por supuesto, el contenido en sí de la herramienta, tienen que poder modificarse según la herramienta que estemos usando. Por este motivo, declaramos las siguientes etiquetas en su respectivo lugar del código html.

	{% block morestyle %}{% endblock %}
	{% block tags %}{% endblock %}
	{% block titulo_barra %}{% endblock %}
	{% block content %}{% endblock %}

Siguiendo con la jerarquia presentada anteriormente, a continuación nos encontramos con la plantilla `generic.html`, que define la estructura general que tendrá la sección que contendrá la información de la herramienta. Ésta hereda de la `base.html` mediante la etiqueta `{% extends 'base.html' %}` y, además, añade otras dos etiquetas dentro de la etiqueta content.

	{% extends 'base.html' %}
	{% block morestyle %}{% endblock %}
	{% block tags %}{% endblock %}
	{% block titulo_barra %}{% endblock %}
	{% block content %}
		<h3>Ejecucion</h3>
			{% block ejecucion %}{% endblock %}

		<h3>Descripcion</h3>
			<p>{% block descripcion %}{% endblock %}</p>
	{% endblock %}

En este caso, "solo" se aprovecha (o mejor dicho "sobreescribe") la etiqueta `content` para modificar el contenido. El resto de etiquetas se definen pero simplemente para que esten disponibles para sus plantillas hijas. Por esta razón, ésta plantilla podría ser omitida ya que no aporta mucho código específico aunque nos podria ser muy util si en un momento dado quisieramos modificar drásticamente la estructura de la sección de cada herramienta (actualmente con dos apartados: Ejecución y Descripción).

En éste mismo nivel de jerarquia, nos encontramos con la plantilla `search_tool.html`, que nos proporciona el diseño que tendrá la página cuando el cliente utilize el buscador. En este caso, como no habrà mas plantillas hijas no se definen las etiquetas vacías, solo las que se sobreescriben.

	{% extends 'base.html' %}
	{% block titulo_barra %} Buscador {% endblock %}
	{% block content %}
	<p style="font-size: 15px;">RESULTADOS DE LA BUSQUEDA:</p>
	<hr/>
	{% for tool in tools %}
	<li>
		<a href="{{tool.link}}">{{tool.titulo}}</a> --> {{tool.descripcion}}
	</li>
	<br/>
	{% endfor %}
	{% endblock %}

Finalmente, en el último nivel de jerarquia, nos encontramos con las plantillas especificas de cada herramienta que heredan de `generic.html`(+ `base.html`). En éstas se extienden, si es necesario, todas las demas etiquetas y se añaden otro tipo de etiquetas que aportan una cierta lógica a la plantilla. Este otro tipo de etiquetas nos permiten modificar dinámicamente el contenido del `.html` que finalmente se mostrará. Las varibles que són evaluadas provienen del diccionario que se ha enviado a través del render de las `views`.

	{% if messages %}
		{% if messages.1 == 'bad' %}
		<div class="alert">
		{% else %}
		<div class="good">
		{% endif %}
		<span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span> 
		{{ messages.0 }}
		</div>
	{% endif %}

	{% for group in groups %}
	  <option value="{{ group }}">{{ group }}</option>
	{% endfor %}

	<label>TABLA: {{table}}</label>

Es necesario remarcar que tal y como se ha comentado, una etiqueta sobreescribe a la anterior. Esto quiere decir que si en la plantilla `base.html` definimos una etiqueta "et" con contenido en su interior {% block et %}prueba{% endblock %} ésta etiqueta sólo sera extendida con el valor "prueba" si no hay ninguna plantilla hija que la sobreescriba. En este segundo caso, si la plantilla `generic.html` también tuviese ésta etiqueta con un valor diferente, al final se mostraría éste último valor.

#### 3.1.3 Subcarpeta: static

En esta carpeta se guardan todos los ficheros estáticos que la página web necesita (imagenes, *css*, fuentes, *javascript*, etc.). La ubicación de ésta carpeta tiene que estar correctamente definida en las variables del *settings.py*, tal y como se ha comentado en apartados anteriores.

### 3.2 Carpeta: for_developing

Ésta carpeta contiene algunos *scripts* y información que nos ayudan a desarrollar sobre la aplicación. Són los siguientes:

- *install_guitool.sh* --> Sirve para desplegar la aplicación de forma rápida en el sistema.
	- Instala (si aún no se disponen) los siguiente paquetes: git, python2-virtualenv, python3, google-chrome-stable.x86_64, screen. 
	- Crea las siguientes carpetas: /etc/guitool, ~/Backups_guitool.
	- Crea los siguientes alias del usuario root:
		- `workon`: levanta el entorno virtual necesario para ejecutar el proyecto.
		- `guitool_start`: arranca la plataforma en modo usuario.
		- `project_dir`: cambia al directorio de trabajo del proyecto.
		- `guitool_developing`: arranca la plataforma a modo de desarrollador.
		- `guitool_stop`: apaga el servidor de Django.

- *sheet_commands.sh* --> *cheat_sheet* de comandos para algunos aspectos de desarrollo como por ejemplo como crear entornos virtuales con *python3*, crear un proyecto de Django, crear un superuser para el Django admin, etc.
- *start_developing.sh* --> *script* que forma parte de uno de los pasos que se realizan al llamar al alias `guitool_developing` y que activa el servidor de Django en una terminal en primer plano para finalmente permitir la puesta en marcha de la plataforma a modo de desarrollador: venv activado, navegador abierto, terminal abierto con el log del servidor Django y directorio de trabajo `/etc/guitool/GUIT_LinuxAdministration`)

### 3.3 Carpeta: script_programs

Contiene los *scripts* que se ejecutan quando una herramienta de la web los requiere para realizar su función de modificación sobre el sistema.

#### create_user
1. Recoge todos los argumentos recibidos.
2. Según que opciones han sido introducidas entre grupos principales y secundarios, ejecuta una orden de bash o la otra. Esto es así ya que no se permiten introducir argumentos vacíos en la orden `useradd`.
3. Por cada campo `GECOS` que permite cambiar la herramienta realiza el cambio si el usuario ha especificado algún valor en el formulario.
4. Establece la *password* al usuario.

#### create backups
1. Recoge todos los argumentos recibidos. Crea el nombre que tendrá el fichero según la fecha y *hardcodea* el directorio dónde se guardará la copia de seguridad.
2. Comprueba que los directorios origen y destino existan en el sistema.
3. Ejecuta el backup llamando al *script* `run_backup` si el usuario ha seleccionado la opcion "Crear ahora".
4. Según el timing que se haya seleccionado, crea el string que sera añadido en la columna de tiempo del crontab.
5. Comprueba que no exista un mismo cron: mismo `timing` y mismo directorio origen.
6. Crea el cron.

#### create_docker
1. Recoge todos los argumentos recibidos.
2. Haz un `pull` de la imagen Docker. Puede que no exista si el usuario ha introducida una personalizada. Controla este error.
3. Establece que el directorio de trabajo sea `/opt/docker` si el usuario no ha especificado ninguno.
4. Establece que la varibale `to_install` sea un string vacío si el usuario no ha especificado ningún paquete a instalar. Necesario para que no se incorpore información durante la creación del `Dockerfile`.
5. Elimina las imagenes anteriores de "dockerguitool" (la imagen que se usa para crear los *containers*). Puede que no exista, pero no hay problema.
6. Crea el `Dockerfile` y guardalo en `/etc/guitool/tmp/Dockerfile`.
7. Cambia a esta carpeta y haz un `build` en este contexto.
8. Crea un *container* de ejemplo para checkear que el nombre o hostname de este *container* no exista y que por tanto puede crearse.
9. Crea el *container* en una nueva instancia del terminal.
10. Vacía el contexto para el siguiente uso de la herramienta.


#### create_bbdd
1. Recoge todos los argumentos recibidos.
2. Exporta la variable `PGPASSWORD` con la información de la password del usuario. Necesario para poder ejecutar *psql* por commando *non-interactive*.
3. Crea la base de datos y guarda los *logs* de error en `/tmp/guitool_bbdd_logs.log`.
4. Mediante un `grep` y estos *logs*, recoje y guarda los errores que puedan haber surgido. Éstos serán mostrados en la plataforma mediante un *banner*.

### 3.4 Carpeta: venv

Es el entorno virtual en el que se ejecuta el servidor de Django y, por tanto, el proyecto entero.

### 3.5 Carpeta: documentacion

Carpeta que contiene toda la documentación generada hasta el momento para éste proyecto:
	- Guia para desarrolladores: Información útil por si se desea ayudar en el desarrollo de la herramienta.
	- Guia para usuarios: Información útil por si se desea simplemente usar la herramienta.
	- Un FAQ (Preguntas y respuestas habituales).
	- Una presentacion de diapositivas que resumen el proyecto.

### 3.6 Otros

En esta carpeta base de nuestro repositorio, además de las carpetas anteriormente comentadas y del fichero *.gitignore* y *README.md*, se añaden algunos *scripts* que permiten la puesta en marcha de la herramienta de una manera facil para los usuarios que solamente quieran utilizarla y no desarrollar sobre ella y un fichero que contiene las instalaciones que se realizan en el entorno virtual cuando se crea. 
- *start_app.sh* --> *script* que forma parte de uno de los pasos que se realizan al llamar al alias guitool_start y que activa el servidor de Django en una terminal en segundo plano para finalmente permitir la puesta en marcha de la plataforma a modo de usuario: solo se visualiza el navegador.
- *activate_env.sh* --> *script* con una sola orden que permite levantar el entorno virtual. Serà llamado durante los procesos de arranque de la plataforma.
- *requirements.txt* --> contiene el software necesario que se instala en el interior del entorno virutal.

## 4. Configuracion de Django Admin y del buscador

Uno de los puntos fuertes de Django es el panel de adminsitración que trae por defecto [127.0.0.1:8000/admin](http://127.0.0.1:8000/admin). Es un panel 100% configurable a los gustos de diseño y necesidades funcionales. En el caso de GUI-Tool for Linux System Administration se ha usado esta aplicación para poder realizar un buscador de herramientas. Los pasos que se han seguido son los siguientes:

- Establecer una password para poder acceder a éste: `python manage.py changepassword root`

- Crear el fichero *models.py*, en el que se establecen los campos que contendrá el formulario donde se definen las herramientas que estan disponibles. Además, se establece el tipo de datos de cada uno, así como algunas opciones como valores por defecto y longitudes máximas. También se podrian añadir opciones de ordenación, asi como funciones extras como la de `__str__` que permite indicarle al Django Admin cual es el nombre del objeto para que nos lo muestre así en su panel. Todo esto modificaría el diseño del formulario en Django Admin y la visualzación/consulta de estos datos a través de él.
	
	from django.db import models
	class Herramientas(models.Model):
	    titulo = models.CharField(max_length=100)
	    descripcion = models.TextField(max_length=1000)
	    link = models.CharField(max_length=150, default='none')
	    data_publicacio = models.DateField()
	    def __str__(self):
	        return self.titulo

- Crear el fichero *admin.py*, donde simplemente se registra la classe `Herramientas` en el administrador para que tenga constancia de ella y aparezca en el panel.

	from django.contrib import admin
	from .models import Herramientas
	admin.site.register(Herramientas)

- Crear la plantilla *search_tool.html* (ya explicada en puntos anteriores)

- Migrar el diseño de la bbdd al servidor con la ayuda de *manage.py*: `python manage.py makemigrations` y `python manage.py migrate`.

- Crear las herramientas desde [/admin/GUIT_LinuxAdministration/herramientas/add/](http://127.0.0.1:8000/admin/GUIT_LinuxAdministration/herramientas/add/)

- Crear la *view* dónde se realiza la consulta a la base de datos y dónde se develve la variable `tools` a la plantilla. Ésta variable será una lista con la información de las herramientas que contengan en el campo descripción el texto introducido en el textfield del buscador: `tools = Herramientas.objects.filter(descripcion__icontains=query).values()` (explicado en apartados anteriores)

## 5. Workflow recomendado para desarrollar una nueva herramienta

A continuación se detalla una recomendación general sobre la "receta" de los pasos que se tendrían que seguir para ayudar en el desarrollo del proyecto GUI-Tool y crear una nueva herramienta:

0. Estudiar las tecnologias específicas que requiere la nueva herramienta.
1. Pensar el diseño web y como incorporarlo a la estructura de plantillas existente.
2. Crear el apartado web específico de esta herramienta e integrarla al código HTML.
3. Crear la lógica interna de la herramienta (*script* modificador del sistema).
4. Crear la logica de Django *view* y integrarla con el codigo `html` y con el *script* modificador del sistema.
5. Comprobar su ejecución y pulir los detalles.
6. Rehacer en bucle los pasos anteriores por las otras herramientas que se desarrollen: Crear el apartado web específico de la herramienta, crear la lógica interna de la herramienta, crear la view de Django y realizar las comprobaciones y modificaciones necesarias.
7. Documentar en todo momento.

## 6. Posibles ampliaciones del proyecto

No solo es possible participar del proyecto GUI-Tool desarrollando nuevas herramientas o mejorando las existentes. A continuación se exponen algunas ideas que servirían como posibles ampliaciones del proyecto ya que pueden ser interesantes para algunos desarrolladores:

- Desarrollar un *rollback* (script de desinstalación para borrar todas las configuraciones generadas durante la instalación) para cuando el administrador quisiera dejar de utilizar definitivamente la herramienta.

- En el supuesto de que el sistema no cumpla los requerimientos de alguna herramienta concreta, dar la opción desde
la misma plataforma web para satisfacerlos. Por ejemplo, si no tiene Docker instalado, dar la opción de instalarlo desde la misma pagina web.

- Migrar todo el proyecto a un servidor web como Apache mediante mod_python. De este modo se evitaría la utilización del servidor web que incorpora Django, puesto que este está orientado únicamente a la realización de pruebas y a trabajar en la etapa de desarrollo.

- Contemplar la manera de gestionar los posibles errores (no controlados previamente por los requerimientos) en la ejecución de alguna herramienta. 

- Bifurcar la plataforma en dos: herramientas para usuarios simples y herramientas para administradores (se tendría que incorporar algun login para identificar que tipo de usuario es)

- Hacer la aplicación multiplataforma: Ahora mismo GUI-Tool solo esta disponible para Fedora. Seria muy interesante el hecho de poder hacer que la aplicación funcione en cualquier sistema operativo Linux.

- Hacer que la herramienta tenga la opción de administrar un host remoto, para poder administrar el host del trabajo desde nuestra casa accediento a la aplicacion web.
